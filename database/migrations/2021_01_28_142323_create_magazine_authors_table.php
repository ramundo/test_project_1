<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagazineAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magazine_authors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('magazine_id');
            $table->unsignedBigInteger('author_id');

            $table->foreign('magazine_id')->references('id')->on('magazines')
                ->onDelete('cascade');
            $table->foreign('author_id')->references('id')->on('authors')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magazine_authors');
    }
}
