<?php

use App\Http\Controllers\API;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('author/add', [API\AuthorController::class, 'store']);
Route::post('author/update/{id}', [API\AuthorController::class, 'update']);
Route::delete('author/delete/{id}', [API\AuthorController::class, 'destroy']);
Route::get('author/list', [API\AuthorController::class, 'index']);

Route::post('magazine/add', [API\MagazineController::class, 'store']);
Route::post('magazine/update/{id}', [API\MagazineController::class, 'update']);
Route::delete('magazine/delete/{id}', [API\MagazineController::class, 'destroy']);
Route::get('magazine/list', [API\MagazineController::class, 'index']);
