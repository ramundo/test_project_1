<?php

namespace App\Http\Controllers\API;

use App\Request\Author\CreateRequest;
use App\Request\Author\UpdateRequest;
use App\Services\Author\CrudService;
use App\Resources\AuthorResource;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as Controller;

class AuthorController extends Controller
{
    /**
     * @var CrudService
     */
    protected $crudService;

    public function __construct(CrudService $crudService)
    {
        $this->crudService = $crudService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse {
        $perPage = $request->input('per_page', 10);
        $authors = $this->crudService->index($perPage);

        return response()->json([
            'message' => 'ok',
            'payload' => AuthorResource::collection($authors),
        ]);
    }

    /**
     * @param CreateRequest $request
     * @return JsonResponse
     */
    public function store(CreateRequest $request): JsonResponse {
        $data = $request->validated();
        $author = $this->crudService->store($data);

        return response()->json([
            'message' => 'ok',
            'payload' => new AuthorResource($author),
        ]);
    }

    /**
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    public function update(UpdateRequest $request): JsonResponse {
        $id = $request->route('id');
        $data = $request->validated();
        $author = $this->crudService->update($id, $data);

        return response()->json([
            'message' => 'ok',
            'payload' => new AuthorResource($author),
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse {
        $id = $request->route('id');
        $this->crudService->destroy($id);

        return response()->json([
            'message' => 'ok',
        ]);
    }
}
