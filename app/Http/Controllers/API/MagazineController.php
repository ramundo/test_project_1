<?php

namespace App\Http\Controllers\API;

use App\Request\Magazine\CreateRequest;
use App\Request\Magazine\UpdateRequest;
use App\Resources\MagazineResource;
use App\Services\Magazine\CrudService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MagazineController extends Controller
{
    /**
     * @var CrudService
     */
    protected $crudService;

    public function __construct(CrudService $crudService)
    {
        $this->crudService = $crudService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse {
        $perPage = $request->input('per_page', 10);
        $magazines = $this->crudService->index($perPage);

        return response()->json([
            'message' => 'ok',
            'payload' => MagazineResource::collection($magazines),
        ]);
    }

    /**
     * @param CreateRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function store(CreateRequest $request): JsonResponse {
        $data = $request->validated();
        $magazine = $this->crudService->store($data);

        return response()->json([
            'message' => 'ok',
            'payload' => new MagazineResource($magazine),
        ]);
    }

    /**
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    public function update(UpdateRequest $request): JsonResponse {
        $id = $request->route('id');
        $data = $request->validated();
        $magazine = $this->crudService->update($id, $data);

        return response()->json([
            'message' => 'ok',
            'payload' => new MagazineResource($magazine),
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse {
        $id = $request->route('id');
        $this->crudService->destroy($id);

        return response()->json([
            'message' => 'ok',
        ]);
    }
}
