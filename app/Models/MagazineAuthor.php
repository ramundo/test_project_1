<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MagazineAuthor extends Model
{
    protected $table = 'magazine_authors';

    protected $casts = [
        'id' => 'integer',
        'magazine_id' => 'integer',
        'author_id' => 'integer',
    ];

    public $timestamps = false;
}