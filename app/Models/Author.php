<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $table = 'authors';

    protected $casts = [
        'name' => 'string',
        'middle_name' => 'string',
        'surname' => 'string',
    ];

    protected $fillable = [
        'name',
        'middle_name',
        'surname',
    ];
}