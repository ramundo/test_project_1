<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Magazine extends Model
{
    protected $table = 'magazines';

    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'image' => 'string',
    ];

    protected $fillable = [
        'name',
        'description',
        'image',
        'released_at',
    ];

    public function authors()
    {
        return $this->hasManyThrough(Author::class, MagazineAuthor::class,
            'magazine_id', 'id', 'id', 'author_id');
    }
}