<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MagazineResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'middle_name' => $this->description,
            'image' => $this->image,
            'released_at' => $this->released_at ? $this->released_at->toIso8601String() : '',
            'updated_at' => $this->updated_at->toIso8601String(),
            'created_at' => $this->created_at->toIso8601String(),
            'authors' => AuthorResource::collection($this->authors)
        ];
    }
}
