<?php


namespace App\Services\Author;

use App\Models\Author;
use Illuminate\Pagination\LengthAwarePaginator;

class CrudService
{
    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function index(int $perPage): LengthAwarePaginator
    {
        return Author::paginate($perPage);
    }

    /**
     * @param array $data
     * @return Author
     */
    public function store(array $data): Author
    {
        $author = new Author();
        $author->fill($data)->save();

        return $author;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Author
     */
    public function update(int $id, array $data): Author
    {
        $author = Author::find($id);
        $author->fill($data)->save();

        return $author;
    }

    /**
     * @param int $id
     */
    public function destroy(int $id): void
    {
        Author::where('id', $id)->delete();
    }
}