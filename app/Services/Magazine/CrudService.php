<?php


namespace App\Services\Magazine;

use App\Models\Magazine;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\File;
use App\Models\MagazineAuthor;

class CrudService
{
    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function index(int $perPage): LengthAwarePaginator
    {
        return Magazine::paginate($perPage);
    }

    /**
     * @param array $data
     * @return Magazine
     * @throws \Exception
     */
    public function store(array $data): Magazine
    {
        $magazine = new Magazine();

        if (isset($data['image'])) {
            $data['image'] = $this->storeImage($data['image']);
        }

        $magazine->fill($data)->save();
        $this->handleAuthors($magazine->getKey(), $data);

        return $magazine;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Magazine
     */
    public function update(int $id, array $data): Magazine
    {
        $magazine = Magazine::find($id);
        $data = $this->handleData($data);
        $magazine->fill($data)->save();
        $this->handleAuthors($magazine->getKey(), $data);

        return $magazine;
    }

    /**
     * @param int $id
     */
    public function destroy(int $id): void
    {
        $magazine = Magazine::find($id);

        if ($magazine) {
            $this->deleteMagazineImage($magazine->image);
            $magazine->delete();
        }
    }

    /**
     * @param string|null $imagePath
     */
    protected function deleteMagazineImage(string $imagePath = null): void
    {
        if ($imagePath) {
            try {
                unlink(base_path() . $imagePath);
            } catch (\Throwable $e) {
                report($e);
            }
        }
    }

    /**
     * @param $file
     * @return string|null
     * @throws \Exception
     */
    protected function storeImage($file): ?string
    {
        if (!$file) {
            return null;
        }

        $basePath = 'app/public/images';
        $destinationPath = storage_path($basePath);
        $extension = substr($file->getClientOriginalName(), strrpos($file->getClientOriginalName(), '.') + 1);
        $name = 'image_' . bin2hex(random_bytes(10)) . '.' . $extension;

        if (!is_dir($destinationPath)) {
            File::makeDirectory($destinationPath, 0755, true);
        }

        $file->move($destinationPath, $name);
        $path = '/storage/' . $basePath . '/' . $name;

        return $path;
    }

    /**
     * @param int $magazineId
     * @param array $data
     */
    protected function handleAuthors(int $magazineId, array $data): void
    {
        if (isset($data['authors_ids'])) {
            MagazineAuthor::where('magazine_id', $magazineId)->delete();

            foreach ($data['authors_ids'] as $authorId) {
                $magazineAuthor = new MagazineAuthor();
                $magazineAuthor->magazine_id = $magazineId;
                $magazineAuthor->author_id = $authorId;
                $magazineAuthor->save();
            }
        }
    }
}