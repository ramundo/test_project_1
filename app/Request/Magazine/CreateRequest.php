<?php

namespace App\Request\Magazine;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'string|max:255|nullable',
            'image' => 'image|mimes:jpeg|max:2048',
            'released_at' => 'date_format:Y-m-d H:i|nullable',
            'authors_ids' => 'array',
            'authors_ids.*' => 'exists:authors,id',
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}

